<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Daaner\TikTok\Models\UserInfo;
use Daaner\TikTok\Models\DiscoverInfo;
use Daaner\TikTok\Models\TagInfo;

class tiktokController extends Controller
{
    //
    public function index()
    {
        $page = 0;
        $users = \DB::table('competition')->get();
        // dd($users);
        foreach ($users as $user) {
            try{
                // echo "cek";
                //Count hashtag
                $tt = new TagInfo;
                $tag = $tt->getTag($user->mainHashtag);
                $hashtagUsage = $tag["result"]["challengeInfo"]["stats"]["videoCount"];
                echo $hashtagUsage;
                //ENd count hashtag
        
        
        
                // Get data
                $data = $this->getData($page, $user->mainHashtag, $user->tiktok_username);
                $data = $data[0];
                // dd($data);
                $challenges = $data['challenges'];
                $hashtagId = NULL;
                foreach ($challenges as $challenge) {
                    $hashtag = \DB::table('hashtag')->where('hashtag', $challenge['title'])->first();
                    if ($hashtag != NULL) {
                        $hashtagId = $hashtag->id;
                    }
                }
                //Ambil variable data simpan ke database
                // dd($hashtagId, $data['author']['avatarMedium'], $data['author']['avatarMedium'], $data['desc'], $user->tiktok_username, $data['video']['id']);
                \DB::table('competition')->where('id', $user->id)->update([
                    'hashtagId' => $hashtagId,
                    'photo' => $data['author']['avatarMedium'],
                    'video_id' => $data['video']['id'],
                    // 'video_desc' => $data['desc'],
                    'video_link' => 'https://www.tiktok.com/@' . $user->tiktok_username . '/video/' . $data['video']['id'],
                    'totalLike' => $data['stats']['diggCount'],
                    'totalShare' => $data['stats']['shareCount'],
                    'totalComment' => $data['stats']['commentCount'],
                    'totalPlay' => $data['stats']['playCount'],
                    'hashtagUsage' => $hashtagUsage
                ]);
                echo "Success\n";
                //End simpan
            }
            catch(\Exception $e){
                echo 'Message: ' .$e->getMessage();
            }
        }
    }

    public function getData($page, $mainHashtag, $userName)
    {
        // dd($userName);
        echo "Retrieving data\n";
        $tt = new TagInfo;
        $tag = $tt->getTag($mainHashtag);
        // dd($tag);
        $tagId = $tag['result']['challengeInfo']['challenge']['id'];
        // dd($tagId);
        $tag = $tt->getTagApi($tagId, 30, $page);

        // dd($tag);
        $tag = $tag["result"]["itemList"];
        $collection = collect($tag);
        // $hashtagUsage = $collection->count();

        $data = $collection->where('author.uniqueId', $userName)->values()->all();
        if (sizeof($data)) { // If more than 0
            // Do Something
            // dd($data);
            return $data;
        } else { // If 0
            // Do Something else
            $page = $page++;
            //Recursion function here
            $this->getData($page, $mainHashtag, $userName);
        }
    }

    public function hashtagCount($page, $mainHashtag, $totalHashtag)
    {
        // dd($userName);
        echo "Count data\n";
        $tt = new TagInfo;
        $tag = $tt->getTag($mainHashtag);
        dd($tag);
        $tagId = $tag['result']['challengeInfo']['challenge']['id'];
        $tag = $tt->getTagApi($tagId, 30, $page);



        //Count hashtag usage
        $result = $tag["result"];
        $result = collect($result);
        $hashtagUsage = collect($result["itemList"]);
        if($result["hasMore"] == true){
            $tempHashtagCount = $hashtagUsage->count();
            $totalHashtag = $totalHashtag + $tempHashtagCount;
            $page = $page++;
            $this->hashtagCount($page, $mainHashtag, $totalHashtag);
        }
        $tempHashtagCount = $hashtagUsage->count();
        $totalHashtag = $totalHashtag + $tempHashtagCount;
        return $totalHashtag;
        
        // dd($hashtagUsage->count());
        // End count hashtag usage
    }

}
